package org.scaldingspoon;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import org.scaldingspoon.glucosetracker.R;

public class AddGlucoseActivity extends Activity {
    private DataHelper dataHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_entry);

        dataHelper = new DataHelper(this);
    }

    public void addEntry(View view) {
        EditText editText = (EditText) findViewById(R.id.log_value);

        if (editText.getText().toString() != null && !editText.getText().toString().equals("")) {
            ContentValues values = new ContentValues();
            values.put("log_type", 1);
            values.put("log_value", Integer.valueOf(editText.getText().toString()));

            getContentResolver().insert(DataProvider.CONTENT_URI, values);
        }

        this.finish();
    }
}