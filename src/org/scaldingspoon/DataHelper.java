package org.scaldingspoon;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelper extends SQLiteOpenHelper {
    public static final String GLUCOSE_LOG_TABLE = "glucose_log";

    private static final int DATABASE_VERSION = 3;
    private static final String DATA_DATABASE_NAME = "glucose_log";
    private static final String DATA_TABLE_CREATE = "create table " + GLUCOSE_LOG_TABLE + " (_id integer primary key autoincrement, log_time integer not null default (strftime('%s', 'now')), log_value real, log_type integer)";

    DataHelper(Context context) {
        super(context, DATA_DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATA_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (newVersion != oldVersion) {
            sqLiteDatabase.execSQL("drop table if exists " + GLUCOSE_LOG_TABLE);
            onCreate(sqLiteDatabase);
        }
    }
}
