package org.scaldingspoon;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class DataProvider extends ContentProvider {
    private DataHelper dataHelper;

    private static final String AUTHORITY = "org.scaldingspoon.gt";
    public static final int LOG_DATA = 100;
    public static final int LOG_DATA_ID = 110;

    private static final String DATA_BASE_PATH = "log_data";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + DATA_BASE_PATH);
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/gt-data";
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/gt-data";

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        uriMatcher.addURI(AUTHORITY, DATA_BASE_PATH, LOG_DATA);
        uriMatcher.addURI(AUTHORITY, DATA_BASE_PATH + "/#", LOG_DATA_ID);
    }

    @Override
    public boolean onCreate() {
        dataHelper = new DataHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(DataHelper.GLUCOSE_LOG_TABLE);

        int uriType = uriMatcher.match(uri);
        switch (uriType) {
            case LOG_DATA_ID:
                queryBuilder.appendWhere("_id=" + uri.getLastPathSegment());
                break;
            case LOG_DATA:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI");
        }
        Cursor cursor = queryBuilder.query(dataHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = uriMatcher.match(uri);
        switch (uriType) {
            case LOG_DATA:
                return CONTENT_TYPE;
            case LOG_DATA_ID:
                return CONTENT_ITEM_TYPE;
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        int uriType = uriMatcher.match(uri);
        if (uriType != LOG_DATA) {
            throw new IllegalArgumentException("invalid uri for insert");
        }
        SQLiteDatabase db = dataHelper.getWritableDatabase();
        long newID = db.insertOrThrow(DataHelper.GLUCOSE_LOG_TABLE, null, contentValues);
        if (newID > 0) {
            Uri newUri = ContentUris.withAppendedId(uri, newID);
            getContext().getContentResolver().notifyChange(uri, null);
            return newUri;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
