package org.scaldingspoon;

import android.database.Cursor;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.Date;

public class DateViewBinder implements SimpleCursorAdapter.ViewBinder{
    @Override
    public boolean setViewValue(View view, Cursor cursor, int index) {
        if (index == cursor.getColumnIndex("log_time")) {
            Date date = new Date(cursor.getLong(index) * 1000);
            ((TextView)view).setText(date.toString());
            return true;
        }
        return false;
    }
}
