package org.scaldingspoon;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import org.scaldingspoon.glucosetracker.R;

public class MyActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int LOG_LIST_LOADER = 0x01;

    protected SimpleCursorAdapter mAdapter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ListView listView = (ListView) findViewById(R.id.log_list);

        getLoaderManager().initLoader(LOG_LIST_LOADER, null, this);

        String[] from = new String[]{"log_time", "log_value"};
        int[] to = new int[]{R.id.log_entry_time, R.id.log_entry_value};
        mAdapter = new SimpleCursorAdapter(this.getApplicationContext(), R.layout.log_entry, null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mAdapter.setViewBinder(new DateViewBinder());
        listView.setAdapter(mAdapter);
    }

    public void addEntry(View view) {
        Intent intent = new Intent(this, AddGlucoseActivity.class);
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = new String[]{"_id", "log_time", "log_value"};
        CursorLoader cursorLoader = new CursorLoader(this, DataProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
    }
}
